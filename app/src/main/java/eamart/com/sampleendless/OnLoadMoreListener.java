package eamart.com.sampleendless;

/**
 * Created by android-dev on 1/29/2018.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}
