package eamart.com.sampleendless;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class CardViewMainProductsAdapter extends RecyclerView.Adapter<CardViewMainProductsAdapter.CardViewMainProductsViewHolder> {

    private LayoutInflater mInflater;
    private Context context;
    private ArrayList<CardviewProductsSetGet> cardviewProductsSetGets;
    private ArrayList<CardviewProductsSetGet> copy;
    public View view;
    //private CartItems cartItems;

    //private ArrayList<SpecialOffer> specialOfferArrayList;

    // the serverListSize is the total number of items on the server side,
    // which should be returned from the web request results
    protected int serverListSize = -1;

    // Two view types which will be used to determine whether a row should be displaying
    // data or a Progressbar
    public static final int VIEW_TYPE_LOADING = 0;
    public static final int VIEW_TYPE_ACTIVITY = 1;

    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    //For SQLite
    //private SQLiteDatabaseHandler sqLiteDatabaseHandler;


    public CardViewMainProductsAdapter(Context context1, ArrayList<CardviewProductsSetGet> productsSetGet, RecyclerView recyclerView) {
        cardviewProductsSetGets = productsSetGet;
        context = context1;
        //listener = listener1;
        mInflater = LayoutInflater.from(context);
        //this.productDataInterface = productDataInterface1;
        //sqLiteDatabaseHandler = new SQLiteDatabaseHandler(context.getApplicationContext());
        copy = cardviewProductsSetGets;
        //specialOfferArrayList = specialOffers;

//        //Toast.makeText(context, "o1111111", Toast.LENGTH_LONG).show();
//        if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
//
//            final GridLayoutManager linearLayoutManager = (GridLayoutManager) recyclerView
//                    .getLayoutManager();
//
//            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//                @Override
//                public void onScrolled(RecyclerView recyclerView,
//                                       int dx, int dy) {
//                    super.onScrolled(recyclerView, dx, dy);
//
//                    totalItemCount = linearLayoutManager.getItemCount(); //16
//                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition(); //3
//
////                    Toast.makeText(context, "on scroll" + totalItemCount + lastVisibleItem, Toast.LENGTH_LONG).show();
////
////                    Toast.makeText(context, "total item count + lastvisibleitem" + " " + totalItemCount +  " " + lastVisibleItem, Toast.LENGTH_LONG).show();
//                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {// && MainActivity.offsetProduct < MainActivity.totalLoadedProduct
//                        // End has been reached
//                        // Do something
//
//                        if (onLoadMoreListener != null) {
//                            Toast.makeText(context, "totalcount:"+totalItemCount+" totalvisible: "+lastVisibleItem+" totalThres:"+visibleThreshold, Toast.LENGTH_SHORT).show();
//                            onLoadMoreListener.onLoadMore();
//                        }
//                        loading = true;
//                    }
//
////                    //15  18
////                    //if (loading == false && lastVisibleItem >= (MainActivity.totalLoadedProduct -3)){
////                    if (!loading && MainActivity.totalLoadedProduct <= (lastVisibleItem + visibleThreshold)){
////                        if(onLoadMoreListener != null){
////                            onLoadMoreListener.onLoadMore();
////                        }
////                        loading = true;
////                    }
//                }
//            });
//        }


    }

    @Override
    public CardViewMainProductsAdapter.CardViewMainProductsViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        view = mInflater.inflate(R.layout.cardview_maincat_product_row, viewGroup, false);
        return new CardViewMainProductsViewHolder(view);
    }

    public void setLoaded() {
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    @Override
    public void onBindViewHolder(final CardViewMainProductsAdapter.CardViewMainProductsViewHolder viewHolder, final int position) {

        Picasso.with(context).load(cardviewProductsSetGets.get(position).getCardMainProductImage()).into(viewHolder.iv_cardviewProductImage);
        viewHolder.tv_cardviewProductName.setText(cardviewProductsSetGets.get(position).getCardMainProductName());
        viewHolder.tv_cardviewProductUOM.setText(cardviewProductsSetGets.get(position).getCardMainProductUOM());

        String priceNoDiscount = "$" + cardviewProductsSetGets.get(position).getCardMainProductOriginalPrice();

        Double parsedPromoPrice = Double.parseDouble(cardviewProductsSetGets.get(position).getCardMainProductPromoPrice());
        String origPriceWithDollar = "$" + cardviewProductsSetGets.get(position).getCardMainProductOriginalPrice();
        String promoPriceWithDollar = "$" + cardviewProductsSetGets.get(position).getCardMainProductPromoPrice();

        if (cardviewProductsSetGets.get(position).getCardMainProductDiscountType().equals("0")) {
            viewHolder.tv_cardviewProductOriginalPrice.setVisibility(View.INVISIBLE);
            viewHolder.tv_triangle.setVisibility(View.INVISIBLE);
            viewHolder.tv_cardviewProductDiscount.setVisibility(View.INVISIBLE);
            viewHolder.tv_cardviewProductPromoPrice.setText(priceNoDiscount);
        } else {
            if (parsedPromoPrice > 0) {
                if ((cardviewProductsSetGets.get(position).getCardMainProductPromoStartDate().equals("null")) && (cardviewProductsSetGets.get(position).getCardMainProductPromoEndDate().equals("null"))) {
                    viewHolder.tv_cardviewProductOriginalPrice.setVisibility(View.INVISIBLE);
                    viewHolder.tv_triangle.setVisibility(View.INVISIBLE);
                    viewHolder.tv_cardviewProductDiscount.setVisibility(View.INVISIBLE);
                    viewHolder.tv_cardviewProductPromoPrice.setText(priceNoDiscount);

                } else {
                    String[] dataproduct_promostartdate = cardviewProductsSetGets.get(position).getCardMainProductPromoStartDate().split(" ");
                    String separated_dataproduct_promostartdate = dataproduct_promostartdate[0];

                    String[] dataproduct_promoenddate = cardviewProductsSetGets.get(position).getCardMainProductPromoEndDate().split(" ");
                    String separated_dataproduct_promoenddate = dataproduct_promoenddate[0];

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                    try {
                        Date currentdate = sdf.parse(getDate());
                        Date promostartdate = sdf.parse(separated_dataproduct_promostartdate);
                        Date promoenddate = sdf.parse(separated_dataproduct_promoenddate);
                        if (currentdate.after(promostartdate) && promoenddate.after(currentdate)) {

                            //for the Discount type 1 with SAVE $15
                            if (cardviewProductsSetGets.get(position).getCardMainProductDiscountType().equals("1")) {
                                String sale = "SAVE " + "$" + cardviewProductsSetGets.get(position).getCardMainProductSale();

                                //change the color of discount
                                viewHolder.tv_triangle.setBackgroundResource(R.drawable.ribbon_discount_orange);
                                viewHolder.tv_cardviewProductDiscount.setBackgroundColor(Color.parseColor("#f44336"));

                                viewHolder.tv_cardviewProductDiscount.setText(sale);
                                viewHolder.tv_cardviewProductOriginalPrice.setText(origPriceWithDollar);
                                viewHolder.tv_cardviewProductPromoPrice.setText(promoPriceWithDollar);

                            } else {
                                viewHolder.tv_cardviewProductDiscount.setText(cardviewProductsSetGets.get(position).getCardMainProductDiscountWholeNo());
                                viewHolder.tv_cardviewProductOriginalPrice.setText(origPriceWithDollar);
                                viewHolder.tv_cardviewProductPromoPrice.setText(promoPriceWithDollar);
                            }

                        } else {
                            viewHolder.tv_cardviewProductOriginalPrice.setVisibility(View.INVISIBLE);
                            viewHolder.tv_triangle.setVisibility(View.INVISIBLE);
                            viewHolder.tv_cardviewProductDiscount.setVisibility(View.INVISIBLE);
                            viewHolder.tv_cardviewProductPromoPrice.setText(priceNoDiscount);
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }


    @Override
    public int getItemCount() {
        return cardviewProductsSetGets.size();
    }

    public class CardViewMainProductsViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_cardviewProductImage;
        private TextView tv_triangle, tv_cardviewProductDiscount, tv_cardviewProductName, tv_cardviewMainProduct_main_quantity, tv_cardviewProductUOM, tv_cardviewProductOriginalPrice, tv_cardviewProductPromoPrice;
        private LinearLayout linear_cardviewMainProduct_main, linear_cardViewMainProduct_addToCart;
        private RelativeLayout relative_cardviewMainProduct_addToCart;
        private RatingBar rb_cardviewProductRating;
        private ImageView iv_maincat_product_minus, iv_maincat_product_add, iv_maincat_product_favorites;
        private TextView tv_cardviewMainProduct_addToCart;


        public CardViewMainProductsViewHolder(View itemView) {
            super(itemView);

            tv_triangle = itemView.findViewById(R.id.tv_maincat_product_triangle);
            iv_cardviewProductImage = itemView.findViewById(R.id.iv_maincat_product_image);
            tv_cardviewProductDiscount = itemView.findViewById(R.id.tv_maincat_product_discount);
            rb_cardviewProductRating = itemView.findViewById(R.id.rb_maincat_product_rating);
            tv_cardviewProductName = itemView.findViewById(R.id.tv_maincat_product_name);
            tv_cardviewProductUOM = itemView.findViewById(R.id.tv_maincat_product_uom);
            tv_cardviewProductOriginalPrice = itemView.findViewById(R.id.tv_maincat_product_slide_originalprice);
            tv_cardviewProductOriginalPrice.setPaintFlags(tv_cardviewProductOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            tv_cardviewProductPromoPrice = itemView.findViewById(R.id.tv_maincat_product_promoprice);
            tv_cardviewMainProduct_addToCart = itemView.findViewById(R.id.tv_maincat_product_addToCart);

            linear_cardViewMainProduct_addToCart = itemView.findViewById(R.id.linear_maincat_product_addToCart);
            relative_cardviewMainProduct_addToCart = itemView.findViewById(R.id.relative_maincat_product_addToCart);

            iv_maincat_product_minus = itemView.findViewById(R.id.iv_maincat_product_minus);
            iv_maincat_product_add = itemView.findViewById(R.id.iv_maincat_product_add);
            tv_cardviewMainProduct_main_quantity = itemView.findViewById(R.id.tv_qty_count);
            iv_maincat_product_favorites = itemView.findViewById(R.id.iv_maincat_product_favorites);

            //end of public CardViewMainProductsViewHolder(View itemView)
        }
    }

    public String getDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date todayDate = new Date();
        String thisDate = simpleDateFormat.format(todayDate);
        return thisDate;
    }





}
