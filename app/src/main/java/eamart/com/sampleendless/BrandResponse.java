package eamart.com.sampleendless;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by android-dev on 11/28/2017.
 */

public class BrandResponse {

    @SerializedName("data")
    @Expose
    private List<DataProducts> dataProducts;

    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("category_name")
    @Expose
    private String category_name;

    @SerializedName("category_main")
    @Expose
    private String category_main;

    @SerializedName("total")
    @Expose
    private String total;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCategory_main() {
        return category_main;
    }

    public void setCategory_main(String category_main) {
        this.category_main = category_main;
    }

    public List<DataProducts> getDataProducts() {
        return dataProducts;
    }

    public void setDataProducts(List<DataProducts> dataProducts) {
        this.dataProducts = dataProducts;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
