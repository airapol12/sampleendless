package eamart.com.sampleendless;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by android-dev on 11/28/2017.
 */

public interface NewRetrofitApiService {

        //FOr FilterRetrofit Java
        @POST("mobile/category_api")
        @FormUrlEncoded
        Call<BrandResponse> getbrandresponse(@Field("type") String type, @Field("limit") String limit, @Field("c_id") String c_id, @Field("c_main") String c_main,
                                             @Field("c_slug") String c_slug, @Field("c_s_slug") String c_s_slug, @Field("c_origin") String c_origin, @Field("brand_id") String brand_id,
                                             @Field("offset") String offset, @Field("bread_crumb") String bread_crumb, @Field("page_sort") String page_sort);
}
