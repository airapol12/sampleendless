package eamart.com.sampleendless;

/**
 * Created by android-dev on 11/28/2017.
 */

public class NewRetrofitApiUtils {
    public static final String BASE_URL = "https://www.eamart.com/";

    public static NewRetrofitApiService getnewService() {
        return RetrofitClient.getClient(BASE_URL).create(NewRetrofitApiService.class);
    }
}
