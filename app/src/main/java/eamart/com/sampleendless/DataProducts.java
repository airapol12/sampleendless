package eamart.com.sampleendless;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by android-dev on 11/23/2017.
 */

public class DataProducts {

    @SerializedName("product_id")
    @Expose
    private String product_id;

    @SerializedName("supplier_id")
    @Expose
    private String supplier_id;

    @SerializedName("product_name")
    @Expose
    private String product_name;

    @SerializedName("brand_name")
    @Expose
    private String brand_name;

    @SerializedName("category_id")
    @Expose
    private String category_id;

    @SerializedName("product_ratings")
    @Expose
    private String product_ratings;

    @SerializedName("product_seo")
    @Expose
    private String product_seo;

    @SerializedName("product_uom")
    @Expose
    private String product_uom;

    @SerializedName("product_delivery_on")
    @Expose
    private String product_delivery_on;

    @SerializedName("product_nextdayafterdeliv")
    @Expose
    private String product_nextdayafterdeliv;

    @SerializedName("product_out_of_stock")
    @Expose
    private String product_out_of_stock;

    @SerializedName("brand_id")
    @Expose
    private String brand_id;

    @SerializedName("product_collection")
    @Expose
    private String product_collection;

    @SerializedName("product_promoStartDate")
    @Expose
    private String product_promoStartDate;

    @SerializedName("product_promoEndDate")
    @Expose
    private String product_promoEndDate;

    @SerializedName("product_launchDate")
    @Expose
    private String product_launchDate;

    @SerializedName("product_slug")
    @Expose
    private String product_slug;

    @SerializedName("product_promo_price")
    @Expose
    private String product_promo_price;

    @SerializedName("product_promo_quantity")
    @Expose
    private String product_promo_quantity;

    @SerializedName("product_original_price")
    @Expose
    private String product_original_price;

    @SerializedName("product_staff_price")
    @Expose
    private String product_staff_price;

    @SerializedName("product_discount_type")
    @Expose
    private String product_discount_type;

    @SerializedName("product_promomech")
    @Expose
    private String product_promomech;

    @SerializedName("product_promomech_label")
    @Expose
    private String product_promomech_label;

    @SerializedName("product_buyx")
    @Expose
    private String product_buyx;

    @SerializedName("product_stock_consistency")
    @Expose
    private String product_stock_consistency;

    @SerializedName("product_lbd")
    @Expose
    private String product_lbd;

    @SerializedName("product_promo_code")
    @Expose
    private String product_promo_code;

    @SerializedName("product_photo")
    @Expose
    private String product_photo;

    @SerializedName("product_photo_optimize")
    @Expose
    private String product_photo_optimize;

    @SerializedName("sale")
    @Expose
    private String sale;

    @SerializedName("product_lead_time")
    @Expose
    private String product_lead_time;

    @SerializedName("stock")
    @Expose
    private String stock;

    @SerializedName("product_qtyhand")
    @Expose
    private String product_qtyhand;

    @SerializedName("onsale")
    @Expose
    private String onsale;

    @SerializedName("savings")
    @Expose
    private String savings;

    @SerializedName("price_sort")
    @Expose
    private String price_sort;

    @SerializedName("margin")
    @Expose
    private String margin;


    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getSupplier_id() {
        return supplier_id;
    }

    public void setSupplier_id(String supplier_id) {
        this.supplier_id = supplier_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getProduct_ratings() {
        return product_ratings;
    }

    public void setProduct_ratings(String product_ratings) {
        this.product_ratings = product_ratings;
    }

    public String getProduct_seo() {
        return product_seo;
    }

    public void setProduct_seo(String product_seo) {
        this.product_seo = product_seo;
    }

    public String getProduct_uom() {
        return product_uom;
    }

    public void setProduct_uom(String product_uom) {
        this.product_uom = product_uom;
    }

    public String getProduct_delivery_on() {
        return product_delivery_on;
    }

    public void setProduct_delivery_on(String product_delivery_on) {
        this.product_delivery_on = product_delivery_on;
    }

    public String getProduct_nextdayafterdeliv() {
        return product_nextdayafterdeliv;
    }

    public void setProduct_nextdayafterdeliv(String product_nextdayafterdeliv) {
        this.product_nextdayafterdeliv = product_nextdayafterdeliv;
    }

    public String getProduct_out_of_stock() {
        return product_out_of_stock;
    }

    public void setProduct_out_of_stock(String product_out_of_stock) {
        this.product_out_of_stock = product_out_of_stock;
    }

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public String getProduct_collection() {
        return product_collection;
    }

    public void setProduct_collection(String product_collection) {
        this.product_collection = product_collection;
    }

    public String getProduct_promoStartDate() {
        return product_promoStartDate;
    }

    public void setProduct_promoStartDate(String product_promoStartDate) {
        this.product_promoStartDate = product_promoStartDate;
    }

    public String getProduct_promoEndDate() {
        return product_promoEndDate;
    }

    public void setProduct_promoEndDate(String product_promoEndDate) {
        this.product_promoEndDate = product_promoEndDate;
    }

    public String getProduct_launchDate() {
        return product_launchDate;
    }

    public void setProduct_launchDate(String product_launchDate) {
        this.product_launchDate = product_launchDate;
    }

    public String getProduct_slug() {
        return product_slug;
    }

    public void setProduct_slug(String product_slug) {
        this.product_slug = product_slug;
    }

    public String getProduct_promo_price() {
        return product_promo_price;
    }

    public void setProduct_promo_price(String product_promo_price) {
        this.product_promo_price = product_promo_price;
    }

    public String getProduct_promo_quantity() {
        return product_promo_quantity;
    }

    public void setProduct_promo_quantity(String product_promo_quantity) {
        this.product_promo_quantity = product_promo_quantity;
    }

    public String getProduct_original_price() {
        return product_original_price;
    }

    public void setProduct_original_price(String product_original_price) {
        this.product_original_price = product_original_price;
    }

    public String getProduct_staff_price() {
        return product_staff_price;
    }

    public void setProduct_staff_price(String product_staff_price) {
        this.product_staff_price = product_staff_price;
    }

    public String getProduct_discount_type() {
        return product_discount_type;
    }

    public void setProduct_discount_type(String product_discount_type) {
        this.product_discount_type = product_discount_type;
    }


    public String getProduct_promomech() {
        return product_promomech;
    }

    public void setProduct_promomech(String product_promomech) {
        this.product_promomech = product_promomech;
    }

    public String getProduct_promomech_label() {
        return product_promomech_label;
    }

    public void setProduct_promomech_label(String product_promomech_label) {
        this.product_promomech_label = product_promomech_label;
    }

    public String getProduct_buyx() {
        return product_buyx;
    }

    public void setProduct_buyx(String product_buyx) {
        this.product_buyx = product_buyx;
    }

    public String getProduct_stock_consistency() {
        return product_stock_consistency;
    }

    public void setProduct_stock_consistency(String product_stock_consistency) {
        this.product_stock_consistency = product_stock_consistency;
    }

    public String getProduct_lbd() {
        return product_lbd;
    }

    public void setProduct_lbd(String product_lbd) {
        this.product_lbd = product_lbd;
    }

    public String getProduct_promo_code() {
        return product_promo_code;
    }

    public void setProduct_promo_code(String product_promo_code) {
        this.product_promo_code = product_promo_code;
    }

    public String getProduct_photo() {
        return product_photo;
    }

    public void setProduct_photo(String product_photo) {
        this.product_photo = product_photo;
    }

    public String getProduct_photo_optimize() {
        return product_photo_optimize;
    }

    public void setProduct_photo_optimize(String product_photo_optimize) {
        this.product_photo_optimize = product_photo_optimize;
    }

    public String getSale() {
        return sale;
    }

    public void setSale(String sale) {
        this.sale = sale;
    }

    public String getProduct_lead_time() {
        return product_lead_time;
    }

    public void setProduct_lead_time(String product_lead_time) {
        this.product_lead_time = product_lead_time;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getProduct_qtyhand() {
        return product_qtyhand;
    }

    public void setProduct_qtyhand(String product_qtyhand) {
        this.product_qtyhand = product_qtyhand;
    }

    public String getOnsale() {
        return onsale;
    }

    public void setOnsale(String onsale) {
        this.onsale = onsale;
    }

    public String getSavings() {
        return savings;
    }

    public void setSavings(String savings) {
        this.savings = savings;
    }

    public String getPrice_sort() {
        return price_sort;
    }

    public void setPrice_sort(String price_sort) {
        this.price_sort = price_sort;
    }

    public String getMargin() {
        return margin;
    }

    public void setMargin(String margin) {
        this.margin = margin;
    }


}
