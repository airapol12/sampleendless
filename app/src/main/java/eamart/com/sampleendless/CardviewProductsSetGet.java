package eamart.com.sampleendless;

/**
 * Created by android-dev on 12/7/2017.
 */

public class CardviewProductsSetGet {

    private String cardMainProductId;
    private String cardMainProductName;
    private String cardMainProductImage;
    private String cardMainProductRating;
    private String cardMainProductUOM;
    private String cardMainProductPromoPrice;
    private String cardMainProductOriginalPrice;
    private String cardMainProductDiscountWholeNo;
    private String cardMainProductDiscountType;
    private String cardMainProductPromoStartDate;
    private String cardMainProductPromoEndDate;
    private int cardMainProductQuantity = 0;
    private String cardMainProductPromoMech;
    private String cardMainProductPromoMechLabel;
    private String cardMainProductSale;


    public CardviewProductsSetGet(String cardProductId, String cardProductName, String cardproductImage, String cardProductRating, String cardProductUOM,
                                  String cardProductPromoPrice, String cardProductOriginalPrice, String cardProductDiscountWholeNo, String cardProductDiscountType,
                                  String cardProductPromoStartDate, String cardProductPromoEndDate, int cardProductQuantity, String cardProductPromoMech, String cardProductPromoMechLabel, String cardProductSale){
        cardMainProductId = cardProductId;
        cardMainProductName = cardProductName;
        cardMainProductImage = cardproductImage;
        cardMainProductRating = cardProductRating;
        cardMainProductUOM = cardProductUOM;
        cardMainProductPromoPrice = cardProductPromoPrice;
        cardMainProductOriginalPrice = cardProductOriginalPrice;
        cardMainProductDiscountWholeNo = cardProductDiscountWholeNo;
        cardMainProductDiscountType = cardProductDiscountType;
        cardMainProductPromoStartDate = cardProductPromoStartDate;
        cardMainProductPromoEndDate = cardProductPromoEndDate;
        cardMainProductQuantity = cardProductQuantity;
        cardMainProductPromoMech = cardProductPromoMech;
        cardMainProductPromoMechLabel = cardProductPromoMechLabel;
        cardMainProductSale = cardProductSale;
    }

    public String getCardMainProductId() {
        return cardMainProductId;
    }

    public void setCardMainProductId(String cardMainProductId) {
        this.cardMainProductId = cardMainProductId;
    }
    public String getCardMainProductName() {
        return cardMainProductName;
    }

    public void setCardMainProductName(String cardMainProductName) {
        this.cardMainProductName = cardMainProductName;
    }

    public String getCardMainProductImage() {
        return cardMainProductImage;
    }

    public void setCardMainProductImage(String cardMainProductImage) {
        this.cardMainProductImage = cardMainProductImage;
    }

    public String getCardMainProductRating() {
        return cardMainProductRating;
    }

    public void setCardMainProductRating(String cardMainProductRating) {
        this.cardMainProductRating = cardMainProductRating;
    }

    public String getCardMainProductUOM() {
        return cardMainProductUOM;
    }

    public void setCardMainProductUOM(String cardMainProductUOM) {
        this.cardMainProductUOM = cardMainProductUOM;
    }

    public String getCardMainProductPromoPrice() {
        return cardMainProductPromoPrice;
    }

    public void setCardMainProductPromoPrice(String cardMainProductPromoPrice) {
        this.cardMainProductPromoPrice = cardMainProductPromoPrice;
    }

    public String getCardMainProductOriginalPrice() {
        return cardMainProductOriginalPrice;
    }

    public void setCardMainProductOriginalPrice(String cardMainProductOriginalPrice) {
        this.cardMainProductOriginalPrice = cardMainProductOriginalPrice;
    }

    public String getCardMainProductDiscountWholeNo() {
        return cardMainProductDiscountWholeNo;
    }

    public void setCardMainProductDiscountWholeNo(String cardMainProductDiscountWholeNo) {
        this.cardMainProductDiscountWholeNo = cardMainProductDiscountWholeNo;
    }

    public String getCardMainProductDiscountType() {
        return cardMainProductDiscountType;
    }

    public void setCardMainProductDiscountType(String cardMainProductDiscountType) {
        this.cardMainProductDiscountType = cardMainProductDiscountType;
    }

    public String getCardMainProductPromoStartDate() {
        return cardMainProductPromoStartDate;
    }

    public void setCardMainProductPromoStartDate(String cardMainProductPromoStartDate) {
        this.cardMainProductPromoStartDate = cardMainProductPromoStartDate;
    }

    public String getCardMainProductPromoEndDate() {
        return cardMainProductPromoEndDate;
    }

    public void setCardMainProductPromoEndDate(String cardMainProductPromoEndDate) {
        this.cardMainProductPromoEndDate = cardMainProductPromoEndDate;
    }


    public int getCardMainProductQuantity() {
        return cardMainProductQuantity;
    }

    public void setCardMainProductQuantity(int cardMainProductQuantity) {
        this.cardMainProductQuantity = cardMainProductQuantity;
    }

    public String getCardMainProductPromoMech() {
        return cardMainProductPromoMech;
    }

    public void setCardMainProductPromoMech(String cardMainProductPromoMech) {
        this.cardMainProductPromoMech = cardMainProductPromoMech;
    }

    public String getCardMainProductPromoMechLabel() {
        return cardMainProductPromoMechLabel;
    }

    public void setCardMainProductPromoMechLabel(String cardMainProductPromoMechLabel) {
        this.cardMainProductPromoMechLabel = cardMainProductPromoMechLabel;
    }

    public String getCardMainProductSale() {
        return cardMainProductSale;
    }

    public void setCardMainProductSale(String cardMainProductSale) {
        this.cardMainProductSale = cardMainProductSale;
    }
}
