package eamart.com.sampleendless;

import android.os.Handler;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private NestedScrollView nestedScrollView;
    private CardViewMainProductsAdapter cardViewMainProductsAdapter;
    private ArrayList<CardviewProductsSetGet> productsArrayList;
    private BrandResponse brandresponse;
    private NewRetrofitApiService newRetrofitApiService;
    public String product_url = "https://cdn3.eamart.com/uploads/products/";
    private CardviewProductsSetGet cardviewProductsSetGet;
    private EndlessRecyclerViewScrollListener scrollListener;

    private GridLayoutManager gridLayoutManager;
    public static int totalCountProduct;
    public static int totalLoadedProduct;
    public static int offsetProduct = 0;

    private boolean loading = false;

    protected Handler handler;
    private int visibleThreshold = 5;
    private int pastVisiblesItems, totalItemCount, visibleItemCount;


    String dataproduct_id, dataproduct_name, dataproduct_image, dataproduct_ratings, dataproduct_uom, dataproduct_promoprice, dataproduct_originalprice,
            dataproduct_discount_wholeno, dataproduct_discounttype, dataproduct_promostartdate, dataproduct_promoenddate, dataproduct_promomech, dataproduct_promomechlabel, dataproduct_sale;

    private String type, limit, c_id, c_main, c_slug, c_s_slug, c_origin, brand_id, offset, bread_crumb, page_sort;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        handler = new Handler();

        //initialization for retrofit for Normal login
        newRetrofitApiService = NewRetrofitApiUtils.getnewService();
        getFilterDetails("0", "16", "53", "53", "0", "Automotive", "category", "0", String.valueOf(offsetProduct), "/product/list/all", "0");

//        nestedScrollView = findViewById(R.id.nested);
//        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
//            @Override
//            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//
//            }
//        });
    }

    public void getFilterDetails(final String types, String limits, final String c_ids, final String c_mains, final String c_slugs,
                                 final String c_s_slugs, final String c_origins, final String brand_ids, String offsets, String bread_crumbs, final String page_sorts) {

        this.type = types;
        this.limit = limits;
        this.c_id = c_ids;
        this.c_main = c_mains;
        this.c_slug = c_slugs;
        this.c_s_slug = c_s_slugs;
        this.c_origin = c_origins;
        this.brand_id = brand_ids;
        this.offset = offsets;
        this.bread_crumb = bread_crumbs;
        this.page_sort = page_sorts;


        //Toast.makeText(getApplicationContext(), "on response loob" , Toast.LENGTH_LONG).show();
        Call<BrandResponse> brandcall = newRetrofitApiService.getbrandresponse(type, limit, c_id, c_main, c_slug, c_s_slug, c_origin, brand_id, String.valueOf(offset), bread_crumb, page_sort);
        brandcall.enqueue(new Callback<BrandResponse>() {
            @Override
            public void onResponse(Call<BrandResponse> call, Response<BrandResponse> response) {
                int statusCode = response.code();
                brandresponse = response.body();

                //Toast.makeText(getApplicationContext(), "on response loob", Toast.LENGTH_LONG).show();
                if (response.isSuccessful()) {
                    if (brandresponse.getCode().equals("1")) {

                        productsArrayList = new ArrayList<>();

                        //ProductName, ProductId, ProductRating, ProductUOM, String cardProductPromoPrice, String cardProductOriginalPrice, String cardProductSale)
                        for (int data = 0; data < brandresponse.getDataProducts().size(); data++) {
                            //Toast.makeText(getApplicationContext(), "trigger for loop for dataproducts", Toast.LENGTH_LONG).show();
                            dataproduct_image = brandresponse.getDataProducts().get(data).getProduct_photo();
                            dataproduct_image = product_url + dataproduct_image;

                            double op = Double.parseDouble(brandresponse.getDataProducts().get(data).getProduct_original_price());
                            double sale = Double.parseDouble(brandresponse.getDataProducts().get(data).getSale());
                            double roundoff = (Math.ceil((sale / op) * 100));
                            Double d = Double.valueOf(roundoff);
                            int whole = d.intValue();
                            dataproduct_discount_wholeno = String.valueOf(whole) + "%" + " OFF";

                            dataproduct_name = brandresponse.getDataProducts().get(data).getProduct_name();
                            dataproduct_id = brandresponse.getDataProducts().get(data).getProduct_id();
                            dataproduct_ratings = brandresponse.getDataProducts().get(data).getProduct_ratings();
                            dataproduct_uom = brandresponse.getDataProducts().get(data).getProduct_uom();
                            dataproduct_promoprice = brandresponse.getDataProducts().get(data).getProduct_promo_price();
                            dataproduct_originalprice = brandresponse.getDataProducts().get(data).getProduct_original_price();
                            //dataproduct_originalprice = "$" + dataproduct_originalprice;
                            //dataproduct_promoprice = "$" + dataproduct_promoprice;

                            dataproduct_discounttype = brandresponse.getDataProducts().get(data).getProduct_discount_type();
                            dataproduct_promostartdate = brandresponse.getDataProducts().get(data).getProduct_promoStartDate();
                            dataproduct_promoenddate = brandresponse.getDataProducts().get(data).getProduct_promoEndDate();


                            //Toast.makeText(getApplicationContext(), "dataproduct_discount" + dataproduct_discount, Toast.LENGTH_LONG).show();
                            cardviewProductsSetGet = new CardviewProductsSetGet(dataproduct_id, dataproduct_name, dataproduct_image, dataproduct_ratings, dataproduct_uom,
                                    dataproduct_promoprice, dataproduct_originalprice, dataproduct_discount_wholeno, dataproduct_discounttype, dataproduct_promostartdate, dataproduct_promoenddate, 0, dataproduct_promomech, dataproduct_promomechlabel, dataproduct_sale);
                            productsArrayList.add(cardviewProductsSetGet);

                        }
                        totalCountProduct = Integer.parseInt(brandresponse.getTotal());
                        offsetProduct += 16;
                        totalLoadedProduct += 16;
                        productFunctions();


                    } else {
                        // Error in clicking category
                        Toast.makeText(getApplicationContext(), "Show filter Failed" + brandresponse.getCode(), Toast.LENGTH_LONG).show();
                        //progressBar_products.setVisibility(View.GONE);
                        //((Activity) context).invalidateOptionsMenu();


                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Response not successful", Toast.LENGTH_SHORT).show();
                    //progressBar_products.setVisibility(View.GONE);
                    //((Activity) context).invalidateOptionsMenu();
                }

            }

            @Override
            public void onFailure(Call<BrandResponse> call, Throwable t) {

            }
        });
    }

    public void productFunctions() {
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(false);
        gridLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(gridLayoutManager);
        cardViewMainProductsAdapter = new CardViewMainProductsAdapter(this, productsArrayList, recyclerView);
        //ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(this, R.dimen.item_offset);
        //recyclerView.addItemDecoration(itemDecoration);
        recyclerView.setAdapter(cardViewMainProductsAdapter);

        Toast.makeText(this, "dfdsfdf", Toast.LENGTH_SHORT).show();

        //cardViewMainProductsAdapter.setLoaded();

//        scrollListener = new EndlessRecyclerViewScrollListener(gridLayoutManager) {
//            @Override
//            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
//                Toast.makeText(MainActivity.this, "onload more", Toast.LENGTH_SHORT).show();
//            }
//        };


//
//        cardViewMainProductsAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
//            @Override
//            public void onLoadMore() {
//                //add null , so the adapter will check view_type and show progress bar at bottom
////                productsArrayList.add(null);
////                cardViewMainProductsAdapter.notifyItemInserted(productsArrayList.size() - 1);
//
//                handler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        Call<BrandResponse> brandcall = newRetrofitApiService.getbrandresponse(type, limit, c_id, c_main, c_slug, c_s_slug, c_origin, brand_id, String.valueOf(offsetProduct), bread_crumb, page_sort);
//                        brandcall.enqueue(new Callback<BrandResponse>() {
//                            @Override
//                            public void onResponse(Call<BrandResponse> call, Response<BrandResponse> response) {
//                                int statusCode = response.code();
//                                brandresponse = response.body();
//
//                                //Toast.makeText(getApplicationContext(), "on response loob", Toast.LENGTH_LONG).show();
//                                if (response.isSuccessful()) {
//                                    if (brandresponse.getCode().equals("1")) {
//
//                                        //productsArrayList = new ArrayList<>();
//
//                                        //ProductName, ProductId, ProductRating, ProductUOM, String cardProductPromoPrice, String cardProductOriginalPrice, String cardProductSale)
//                                        for (int data = 0; data < brandresponse.getDataProducts().size(); data++) {
//                                            //Toast.makeText(getApplicationContext(), "trigger for loop for dataproducts", Toast.LENGTH_LONG).show();
//                                            dataproduct_image = brandresponse.getDataProducts().get(data).getProduct_photo();
//                                            dataproduct_image = product_url + dataproduct_image;
//
//                                            double op = Double.parseDouble(brandresponse.getDataProducts().get(data).getProduct_original_price());
//                                            double sale = Double.parseDouble(brandresponse.getDataProducts().get(data).getSale());
//                                            double roundoff = (Math.ceil((sale / op) * 100));
//                                            Double d = Double.valueOf(roundoff);
//                                            int whole = d.intValue();
//                                            dataproduct_discount_wholeno = String.valueOf(whole) + "%" + " OFF";
//
//                                            dataproduct_name = brandresponse.getDataProducts().get(data).getProduct_name();
//                                            dataproduct_id = brandresponse.getDataProducts().get(data).getProduct_id();
//                                            dataproduct_ratings = brandresponse.getDataProducts().get(data).getProduct_ratings();
//                                            dataproduct_uom = brandresponse.getDataProducts().get(data).getProduct_uom();dataproduct_promoprice = brandresponse.getDataProducts().get(data).getProduct_promo_price();
//                                            dataproduct_originalprice = brandresponse.getDataProducts().get(data).getProduct_original_price();
//                                            //dataproduct_originalprice = "$" + dataproduct_originalprice;
//                                            //dataproduct_promoprice = "$" + dataproduct_promoprice;
//
//                                            dataproduct_discounttype = brandresponse.getDataProducts().get(data).getProduct_discount_type();
//                                            dataproduct_promostartdate = brandresponse.getDataProducts().get(data).getProduct_promoStartDate();
//                                            dataproduct_promoenddate = brandresponse.getDataProducts().get(data).getProduct_promoEndDate();
//
//
//                                            //Toast.makeText(getApplicationContext(), "dataproduct_discount" + dataproduct_discount, Toast.LENGTH_LONG).show();
//                                            cardviewProductsSetGet = new CardviewProductsSetGet(dataproduct_id, dataproduct_name, dataproduct_image, dataproduct_ratings, dataproduct_uom,
//                                                    dataproduct_promoprice, dataproduct_originalprice, dataproduct_discount_wholeno, dataproduct_discounttype, dataproduct_promostartdate, dataproduct_promoenddate, 0,dataproduct_promomech, dataproduct_promomechlabel, dataproduct_sale);
//                                            productsArrayList.add(cardviewProductsSetGet);
//                                            cardViewMainProductsAdapter.notifyItemInserted(productsArrayList.size());
//                                        }
//                                        totalCountProduct = Integer.parseInt(brandresponse.getTotal());
//                                        offsetProduct += 16;
//                                        totalLoadedProduct += 16;
//
//
//
//                                    } else {
//                                        // Error in clicking category
//                                        Toast.makeText(getApplicationContext(), "Show filter Failed" + brandresponse.getCode(), Toast.LENGTH_LONG).show();
//                                        //progressBar_products.setVisibility(View.GONE);
//                                        //((Activity) context).invalidateOptionsMenu();
//
//
//                                    }
//                                } else {
//                                    Toast.makeText(getApplicationContext(), "Response not successful", Toast.LENGTH_SHORT).show();
//                                    //progressBar_products.setVisibility(View.GONE);
//                                    //((Activity) context).invalidateOptionsMenu();
//                                }
//
//                                cardViewMainProductsAdapter.setLoaded();
//                            }
//
//                            @Override
//                            public void onFailure(Call<BrandResponse> call, Throwable t) {
//
//                                cardViewMainProductsAdapter.setLoaded();
//                            }
//                        });
//
//                        //or you can add all at once but do not forget to call mAdapter.notifyDataSetChanged();
//                    }
//                }, 2000);
//
//            }
//        });
//


        nestedScrollView = findViewById(R.id.nested);
        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

//                if (v.getChildAt(v.getChildCount() - 1) != null) {
//                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
//                            scrollY > oldScrollY) {
                Double twenty = v.getChildAt(0).getMeasuredHeight() * .20;
                int twent = twenty.intValue();
                if(scrollY > 4000){
                    Toast.makeText(getApplicationContext(), "scfrolly:"+scrollY+" zzzzzzz"+twent+" sdadsa:"+v.getChildAt(0).getMeasuredHeight(), Toast.LENGTH_LONG).show();
                }
                //if (scrollY >= (v.getChildAt(0).getMeasuredHeight() - twent)) {
                if(scrollY > 4000){

                    visibleItemCount = gridLayoutManager.findLastVisibleItemPosition();
                    totalItemCount = gridLayoutManager.getItemCount();
                    pastVisiblesItems = gridLayoutManager.findFirstVisibleItemPosition();
                    Toast.makeText(getApplicationContext(), "loading:"+loading+" offset:"+offsetProduct+" totalproduct:"+totalCountProduct, Toast.LENGTH_LONG).show();

                    if (!loading && offsetProduct < totalCountProduct) {
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Call<BrandResponse> brandcall = newRetrofitApiService.getbrandresponse(type, limit, c_id, c_main, c_slug, c_s_slug, c_origin, brand_id, String.valueOf(offsetProduct), bread_crumb, page_sort);
                                brandcall.enqueue(new Callback<BrandResponse>() {
                                    @Override
                                    public void onResponse(Call<BrandResponse> call, Response<BrandResponse> response) {
                                        int statusCode = response.code();
                                        brandresponse = response.body();

                                        //Toast.makeText(getApplicationContext(), "on response loob", Toast.LENGTH_LONG).show();
                                        if (response.isSuccessful()) {
                                            if (brandresponse.getCode().equals("1")) {

                                                //productsArrayList = new ArrayList<>();

                                                //ProductName, ProductId, ProductRating, ProductUOM, String cardProductPromoPrice, String cardProductOriginalPrice, String cardProductSale)
                                                for (int data = 0; data < brandresponse.getDataProducts().size(); data++) {
                                                    //Toast.makeText(getApplicationContext(), "trigger for loop for dataproducts", Toast.LENGTH_LONG).show();
                                                    dataproduct_image = brandresponse.getDataProducts().get(data).getProduct_photo();
                                                    dataproduct_image = product_url + dataproduct_image;

                                                    double op = Double.parseDouble(brandresponse.getDataProducts().get(data).getProduct_original_price());
                                                    double sale = Double.parseDouble(brandresponse.getDataProducts().get(data).getSale());
                                                    double roundoff = (Math.ceil((sale / op) * 100));
                                                    Double d = Double.valueOf(roundoff);
                                                    int whole = d.intValue();
                                                    dataproduct_discount_wholeno = String.valueOf(whole) + "%" + " OFF";

                                                    dataproduct_name = brandresponse.getDataProducts().get(data).getProduct_name();
                                                    dataproduct_id = brandresponse.getDataProducts().get(data).getProduct_id();
                                                    dataproduct_ratings = brandresponse.getDataProducts().get(data).getProduct_ratings();
                                                    dataproduct_uom = brandresponse.getDataProducts().get(data).getProduct_uom();
                                                    dataproduct_promoprice = brandresponse.getDataProducts().get(data).getProduct_promo_price();
                                                    dataproduct_originalprice = brandresponse.getDataProducts().get(data).getProduct_original_price();
                                                    //dataproduct_originalprice = "$" + dataproduct_originalprice;
                                                    //dataproduct_promoprice = "$" + dataproduct_promoprice;

                                                    dataproduct_discounttype = brandresponse.getDataProducts().get(data).getProduct_discount_type();
                                                    dataproduct_promostartdate = brandresponse.getDataProducts().get(data).getProduct_promoStartDate();
                                                    dataproduct_promoenddate = brandresponse.getDataProducts().get(data).getProduct_promoEndDate();


                                                    //Toast.makeText(getApplicationContext(), "dataproduct_discount" + dataproduct_discount, Toast.LENGTH_LONG).show();
                                                    cardviewProductsSetGet = new CardviewProductsSetGet(dataproduct_id, dataproduct_name, dataproduct_image, dataproduct_ratings, dataproduct_uom,
                                                            dataproduct_promoprice, dataproduct_originalprice, dataproduct_discount_wholeno, dataproduct_discounttype, dataproduct_promostartdate, dataproduct_promoenddate, 0, dataproduct_promomech, dataproduct_promomechlabel, dataproduct_sale);
                                                    productsArrayList.add(cardviewProductsSetGet);
                                                    cardViewMainProductsAdapter.notifyItemInserted(productsArrayList.size());
                                                }
                                                totalCountProduct = Integer.parseInt(brandresponse.getTotal());
                                                offsetProduct += 16;
                                                totalLoadedProduct += 16;


                                            } else {
                                                // Error in clicking category
                                                Toast.makeText(getApplicationContext(), "Show filter Failed" + brandresponse.getCode(), Toast.LENGTH_LONG).show();
                                                //progressBar_products.setVisibility(View.GONE);
                                                //((Activity) context).invalidateOptionsMenu();


                                            }
                                        } else {
                                            Toast.makeText(getApplicationContext(), "Response not successful", Toast.LENGTH_SHORT).show();
                                            //progressBar_products.setVisibility(View.GONE);
                                            //((Activity) context).invalidateOptionsMenu();
                                        }

                                        loading = false;
                                    }

                                    @Override
                                    public void onFailure(Call<BrandResponse> call, Throwable t) {

                                        loading = false;
                                    }
                                });

                                //or you can add all at once but do not forget to call mAdapter.notifyDataSetChanged();
                            }
                        }, 2000);
                    }
                    loading = true;

                }
            }
        });


    }

    private boolean isLoadData() {
        return true;
    }

    public void loadNextDataFromApi(int offset) {
        // Send an API request to retrieve appropriate paginated data
        //  --> Send the request including an offset value (i.e `page`) as a query parameter.
        //  --> Deserialize and construct new model objects from the API response
        //  --> Append the new data objects to the existing set of items inside the array of items
        //  --> Notify the adapter of the new items made with `notifyItemRangeInserted()`
    }

}
